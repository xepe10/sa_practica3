const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const axios = require('axios');

dotenv.config();
app.use(express.json({limit: '50mb'}));
app.use(cors(
    {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Authorization']
    }
));

const pedido = {
    id: 1234,
    estado: 'pendiente',
    producto:{
        nombre: 'Camisa'
    }
}

const router = express.Router();

router.get('/solicitarpedido', async (req, res) => {
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El cliente solicito pedido ${pedido.id}`});
    let rest = await axios.post(`http://localhost:3005/recibirpedidorest`,pedido);
    res.json(rest.data);
});

router.get('/estadorestaurante/:id', async (req, res) => {
    let id = req.params?.id;
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El cliente solicito estado de pedido ${id} a restaurante`});
    let rest = await axios.get(`http://localhost:3005/estadopedidorest/${id}`);
    res.json(rest.data);
});

router.get('/estadorepartidor/:id', async (req, res) => {
    let id = req.params?.id;
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El cliente solicito estado de pedido ${id} a repartidor`});
    let rest = await axios.get(`http://localhost:3005/estadopedidorepart/${id}`);
    res.json(rest.data);
});

app.use(`/`, router);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Create listening at PORT: ${PORT}`);
});