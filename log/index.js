const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const axios = require('axios');

dotenv.config();
app.use(express.json({limit: '50mb'}));
app.use(cors(
    {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Authorization']
    }
));

const pedido = {
    id: 1234,
    estado: 'pendiente',
    producto:{
        nombre: 'Camisa'
    }
}

const router = express.Router();

router.post('/log', (req, res) => {
    let accion = req.body;
    console.log(accion.msj);
    res.json({msj:'ok'});
});

app.use(`/`, router);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Create listening at PORT: ${PORT}`);
});