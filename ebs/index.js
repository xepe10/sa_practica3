const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const axios = require('axios');

dotenv.config();
app.use(express.json({limit: '50mb'}));
app.use(cors(
    {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Authorization']
    }
));

const router = express.Router();

router.get('/solicitarpedido', async (req, res) => {
    let rest = await axios.get(`http://localhost:3001/solicitarpedido`);
    res.json(rest.data);
});

router.get('/estadorestaurante/:id', async (req, res) => {
    let id = req.params?.id;
    let rest = await axios.get(`http://localhost:3001/estadorestaurante/${id}`);
    res.json(rest.data);
});

router.get('/estadorepartidor/:id', async (req, res) => {
    let id = req.params?.id;
    let rest = await axios.get(`http://localhost:3001/estadorepartidor/${id}`);
    res.json(rest.data);
});

router.post('/log', async (req, res) => {
    let accion = req.body;
    let rest = await axios.post(`http://localhost:3004/log`, accion);
    res.json(rest.data);
});

router.post('/recibirpedidorepart', async (req, res) => {
    let pedido = req.body;
    let rest = await axios.post(`http://localhost:3002/recibirpedido`,pedido);
    res.json(rest.data);
});

router.get('/estadopedidorepart/:id', async (req, res) => {
    let id = req.params?.id;
    let rest = await axios.get(`http://localhost:3002/estadopedido/${id}`);
    res.json(rest.data);
});

router.post('/marcarentregado', async (req, res) => {
    let pedido = req.body;
    let rest = await axios.post(`http://localhost:3002/marcarentregado`,pedido);
    res.json(rest.data);
});

router.post('/recibirpedidorest', async (req, res) => {
    let pedido = req.body;
    let rest = await axios.post(`http://localhost:3003/recibirpedido`,pedido);
    res.json(rest.data);
});

router.get('/estadopedidorest/:id', async (req, res) => {
    let id = req.params?.id;
    let rest = await axios.get(`http://localhost:3003/estadopedido/${id}`);
    res.json(rest.data);
});

router.post('/enviarrepartidor', async (req, res) => {
    let pedido = req.body;
    let rest = await axios.post(`http://localhost:3003/enviarrepartidor`,pedido);
    res.json(rest.data);
});

app.use(`/`, router);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Create listening at PORT: ${PORT}`);
});