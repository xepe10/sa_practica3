const express = require('express');
const app = express();
const cors = require('cors');
const dotenv = require('dotenv');
const axios = require('axios');

dotenv.config();
app.use(express.json({limit: '50mb'}));
app.use(cors(
    {
        origin: '*',
        methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
        allowedHeaders: ['Content-Type', 'Authorization']
    }
));

const pedido = {
    id: 1234,
    estado: 'pendiente',
    producto:{
        nombre: 'Camisa'
    }
}

const router = express.Router();

router.post('/recibirpedido', async (req, res) => {
    let pedido = req.body;
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El repartidor recibio el pedido ${pedido.id}`});
    res.json(pedido);
});

router.get('/estadopedido/:id', async (req, res) => {
    let id = req.params?.id;
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El repartidor envio estado de pedido ${id}`});
    res.json({estado: pedido.estado});
});

router.post('/marcarentregado', async (req, res) => {
    let log = await axios.post(`http://localhost:3005/log`,{msj:`El repartidor marco como entregado pedido`});
    res.json({estado: 'entregado'});
});

app.use(`/`, router);

const PORT = process.env.PORT;
app.listen(PORT, () => {
    console.log(`Create listening at PORT: ${PORT}`);
});